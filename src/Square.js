import React from 'react';
import Board from './Board';
import ChessPiece from './ChessPiece'

class Square extends React.Component{
    constructor() {
        super();
        this.state = {
            // not used for now.
        };
    }
    getSquareColor() {
        const pos = Board.getCoordinates(this.props.position);
        return (pos.col + pos.row) % 2 ? "SaddleBrown" : "BurlyWood"
    }
    handleClick(square) {
        // clicked on an square, this.props.piece can be empty (null)
        square.props.onClick(square.props.piece);
    }
    render() {
        const piece = this.props.piece
            ? (
                <ChessPiece
                    piece={this.props.piece}
                />
            )
            : null;
        return (
            <button
                className="square"
                onClick={() => this.handleClick(this)}
                style={{background: this.getSquareColor()}}
            >
                {piece}
            </button>
        );
    }
}

export default Square;