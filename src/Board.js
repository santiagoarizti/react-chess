import React from 'react';
import Square from './Square';
import ChessPiece from './ChessPiece';

class Board extends React.Component {

    static getBoardSize() {
        return 8;
    }
    static getCoordinates(square) {
        const boardSize = Board.getBoardSize();
        const row = Math.floor(square / boardSize) + 1;
        const col = (square % boardSize) + 1;
        return {col: col, row: row};
    }
    static getChessCoordinates(square) {
        const pos = Board.getCoordinates(square);
        // in chess, the bottom row is lowest
        // and columns are letters.
        const colMap = ["", "A", "B", "C", "D", "E", "F", "G", "H"];
        return {col: colMap[pos.col], row: Board.getBoardSize() - pos.row + 1};
    }

    static getStartingBoard() {
        const w = true, b = false;
        const g = ChessPiece.generatePiece;
        const Q = "Q", K = "K", R = "R", B = "B", N = "N", p = "";
        return [
            g(b, R), g(b, N), g(b, B), g(b, Q), g(b, K), g(b, B), g(b, N), g(b, R),
            g(b, p), g(b, p), g(b, p), g(b, p), g(b, p), g(b, p), g(b, p), g(b, p),
            null,    null,    null,    null,    null,    null,    null,    null,
            null,    null,    null,    null,    null,    null,    null,    null,
            null,    null,    null,    null,    null,    null,    null,    null,
            null,    null,    null,    null,    null,    null,    null,    null,
            g(w, p), g(w, p), g(w, p), g(w, p), g(w, p), g(w, p), g(w, p), g(w, p),
            g(w, R), g(w, N), g(w, B), g(w, Q), g(w, K), g(w, B), g(w, N), g(w, R),
        ];
    }
    render() {
        const rowSize = Board.getBoardSize();
        let rows = [];
        for (let row = 0; row < rowSize; row++) {
            let cols = [];
            const iStart = row * rowSize;
            for (let i = iStart; i < iStart + rowSize; i ++) {
                cols.push(
                    <Square key={i}
                            piece={this.props.squares[i]}
                            onClick={(piece) => this.props.onClick(i, piece)}
                            position={i}
                    />
                );
            }
            rows.push(
                <div key={row} className="board-row">
                    {cols}
                </div>
            );
        }

        return (
            <div>
                {rows}
            </div>
        );
    }
}

export default Board;