import React from 'react';

class ChessPiece extends React.Component {
    constructor() {
        super();
    }
    static generatePiece(isWhite, pieceType) {
        return {isWhite: isWhite, pieceType: pieceType}
    };
    render() {
        const spriteSize = -64;
        const imageY = this.props.piece.isWhite ? 0 : 1;
        const imageX = ["K", "Q", "B", "N", "R", ""].indexOf(this.props.piece.pieceType);
        const style = {
            height: "100%",
            backgroundImage: "url('pieces-64.png')",
            backgroundRepeat: "no-repeat",
            backgroundPosition: (imageX * spriteSize) + "px " + (imageY * spriteSize) + "px",
        };
        return (
            <div style={style} >
                {/*this.props.piece.pieceType*/}
            </div>
        );
    }
}

export default ChessPiece;