import React from 'react';
import Board from './Board';

class Game extends React.Component {
    constructor() {
        super();
        this.state = {
            history: [{
                squares: Board.getStartingBoard(),
                clickedSquare: null, // i of square
                whiteTurn: true,
            }],
            stepNumber: 0,
            heldPiece: null, // can have i of square + piece
        };
    }
    handleClick(i, piece) {
        let history = this.state.history.slice(0, this.state.stepNumber + 1);
        const current = history[history.length - 1];
        const squares = current.squares.slice();

        let heldPiece = this.state.heldPiece; // square + piece
        let newHeldPiece = null; // the heldPiece for the next click
        let nextPlayer = current.whiteTurn; // will maybe change if piece is moved.
        let stepNumber = history.length - 1; // not sure if we will advance.

        // helper functions. todo: maybe move to own function.
        const holdOwnPiece = () => {
            newHeldPiece = {i: i, piece: piece};
        };
        const movePieceToSquare = () => {
            // put piece in new location
            squares[i] = heldPiece.piece;
            // remove piece from old location.
            squares[heldPiece.i] = null;
            // advance step number for history keeping
            stepNumber++;
            // add to history
            history = history.concat([{
                squares: squares,
                clickedSquare: i,
                // end turn
                whiteTurn: !nextPlayer
            }]);
        };

        console.log("handling " + i + ", piece: " + (piece ? (piece.isWhite ? "w" : "b") + (piece.pieceType || "i") : "-"));

        if (heldPiece) {
            // player had a piece in their hand and they are selecting
            // a target square.
            // valid squares are empty squares or squares with edible enemies.
            // + squares that are valid moves of a piece.
            // if square has piece owned by player then just hold that one instead.
            if (piece) {
                // player attempting to move piece into an enemy's piece
                // or just selecting another one of his pieces instead
                if (piece.isWhite === current.whiteTurn) {
                    // player owns this piece, just mark it as the currently held piece.
                    holdOwnPiece();
                } else {
                    // piece belongs to other player.
                    // todo: validate if piece can be moved.
                    let canEat = true;
                    if (canEat) {
                        movePieceToSquare();
                    }
                }
            } else {
                // square is empty, move the held piece there
                movePieceToSquare()
            }
        } else {
            // player might be selecting a new piece to move, or just
            // clicking around.
            // piece owned by player selects it.
            // enemy piece or empty square just return without action.
            if (piece) {
                // there is a piece in this square.
                // this square can be used to make the first click, select a piece
                // that will be moved in the next click.
                // check if the piece clicked is owned by the current player.
                if (piece.isWhite === current.whiteTurn) {
                    // player owns this piece, just mark it as the currently held piece.
                    holdOwnPiece();
                } else {
                    // no held piece, and no holdable piece, just ignore this click.
                    return;
                }
            } else {
                // no held piece, and no holdable piece, just ignore this click.
                return;
            }
        }

        this.setState({
            history: history,
            stepNumber: stepNumber,
            heldPiece: newHeldPiece,
        });
    }

    jumpTo(step) {
        this.setState({
            stepNumber: step,
            whiteTurn: !(step % 2),
        });
    }

    static calculateWinner(squares) {
        const lines = [
            [0, 1, 2],
        ];
        for (let i = 0; i < lines.length; i++) {
            const [a, b, c] = lines[i];
            if (squares[a] && squares[a] === squares[b] && squares[a] === squares[c]) {
                return squares[a];
            }
        }
        return null;
    }
    static renderDescription (isWhite, square) {
        const prevIsWhite = !isWhite; // it is phased off by one
        const player = prevIsWhite ? "W" : "B";
        const pos = Board.getChessCoordinates(square);
        return `${player}: ${pos.col}, ${pos.row}`;
    }
    render() {
        const history = this.state.history;
        const current = history[this.state.stepNumber];
        const winner = Game.calculateWinner(current.squares);

        const moves = history.map((step, move) => {
            const desc = move
                ? Game.renderDescription(step.whiteTurn, step.clickedSquare)
                : 'Game start';
            return (
                <li key={move}>
                    <a href="#" onClick={() => this.jumpTo(move)}>{desc}</a>
                </li>
            );
        });

        let status;
        if (winner) {
            status = 'Winner: ' + winner;
        } else {
            status = 'Next player: ' + (this.state.whiteTurn ? 'W' : 'B');
        }

        return (
            <div className="game">
                <div className="game-board">
                    <Board
                        squares={current.squares}
                        onClick={(i, piece) => this.handleClick(i, piece)}
                    />
                </div>
                <div className="game-info">
                    <div>{status}</div>
                    <ol>{moves}</ol>
                </div>
            </div>
        );
    }
}

export default Game;